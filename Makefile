INCLUDE :=solution/include
SOURCE :=solution/source
OBJECT :=obj
TARGET :=dict
COMPILE_FLAGS :=-felf64 -I$(INCLUDE) -g

ASM_SOURCES :=$(wildcard $(SOURCE)/*.asm)
OBJECTS :=$(patsubst $(SOURCE)%,$(OBJECT)%,$(patsubst %.asm,%.o,$(ASM_SOURCES)))

all: $(OBJECTS)
	ld $(OBJECTS) -o $(TARGET)
	@echo "Build $(TARGET)"

$(OBJECT)/%.o: $(SOURCE)/%.asm
	@-mkdir $(OBJECT) 2> /dev/null || true;
	nasm $(COMPILE_FLAGS) $< -o $@

clean:
	rm -rf $(OBJECT)/*

.PHONY: all clean
