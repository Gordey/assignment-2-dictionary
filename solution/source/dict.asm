section .text

%define DECLARE global
%include "dict.inc"

%include "lib.inc"
%include "colon.inc"

find_word:
    .loop:
        cmp rsi, DICT_END           ; if at null pointer
        je .not_found               ; return 0

        push rsi                    ; save current node start

        add rsi, POINTER_SIZE       ; set rsi to be node key start
        safe_call string_equals     ; compare string pointed by rdi to node key

        pop rsi                     ; restore node start

        test rax, rax               ; if not equal
        jz .continue                ; continue

        mov rax, rsi                ; otherwise return current node start
        ret

        .continue:
            mov rsi, [rsi]          ; go to next node
            jmp .loop

    .not_found:
    xor rax, rax
    ret
