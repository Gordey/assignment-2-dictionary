%include "lib.inc"
%include "dict.inc"
%include "words.inc"

global _start

%define WORD_BUFFER_SIZE 256
%define CODE_SUCCESS 0
%define CODE_TOO_LONG 1
%define CODE_NOT_FOUND 2

section .data
    message_enter_key: db 'enter key: ', 0
    message_too_long: db 'key too long', 0
    message_not_found: db 'key not found', 0
    message_value: db 'value: ', 0

section .text

_start:
    sub rsp, WORD_BUFFER_SIZE   ; create word buffer in stack

    mov rdi, message_enter_key  ; ask user to enter key
    safe_call print_string      ;

    mov rdi, rsp                ; set buffer start
    mov rsi, WORD_BUFFER_SIZE   ; set buffer size
    safe_call read_word         ; read word

    test rax, rax               ; no word?
    jz .word_too_long           ; then it's too long

    mov rsi, DICT_START         ; set dict start
    call find_word              ; find word

    cmp rax, DICT_END           ; at dict end?
    jz .word_not_found          ; then it's not found

    push rax                    ;
    mov rdi, message_value      ; prepend value with message
    safe_call print_string      ;
    pop rax                     ;

    mov rdi, rax                ; set rdi to be at next node pointer
    add rdi, POINTER_SIZE       ; set rdi to be at node key start

    safe_call string_length     ; get node key length
    inc rax                     ; don't forget about null terminator

    add rdi, rax                ; set rdi to be at node value start
    mov rsi, CODE_SUCCESS       ; program exit code

    jmp .finish                 ; print node value

    .word_too_long:
    mov rdi, message_too_long   ; set rdi to be at message start
    mov rsi, CODE_TOO_LONG      ; program exit code
    jmp .finish                 ; print message

    .word_not_found:
    mov rdi, message_not_found  ; set rdi to be at message start
    mov rsi, CODE_NOT_FOUND     ; program exit code
    jmp .finish                 ; print message

    .finish:

    safe_call print_string      ; print message
    safe_call print_newline

    mov rdi, rsi                ; set program exit code

    call exit
