%ifndef _LIB_INC_
%define _LIB_INC_

%ifndef DECLARE
    %define DECLARE extern
%endif

DECLARE exit
DECLARE string_length
DECLARE print_string
DECLARE print_char
DECLARE print_newline
DECLARE print_uint
DECLARE print_int
DECLARE string_equals
DECLARE read_char
DECLARE read_word
DECLARE parse_uint
DECLARE parse_int
DECLARE string_copy

%macro safe_call 1
    %ifid %1
        push rdi
        push rsi
        push rdx
        push rcx
        push r8
        push r9
        call %1
        pop r9
        pop r8
        pop rcx
        pop rdx
        pop rsi
        pop rdi
    %else
        %error "%1 must be function name"
    %endif
%endmacro

%undef DECLARE
%endif
