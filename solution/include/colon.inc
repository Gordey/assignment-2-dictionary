%ifndef _COLON_INC_
%define _COLON_INC_
%define DICT_END 0
%define DICT_START DICT_END
%define POINTER_SIZE 8
%macro colon 2
    %ifid %2                                ; if second is an identifier
        %2: dq DICT_START                   ; set pointer to next node for current node
        %define DICT_START %2               ; set new start node
    %else
        %error "%2 is not an identifier"
    %endif
    %ifstr %1                               ; if first is a string
        db %1, 0                            ; set node key
    %else
        %error "%1 is not a string"
    %endif
%endmacro
%endif
